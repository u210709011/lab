
import java.util.Scanner;


public class FindPrimes {
    public static void main(String[] args) {
        Scanner temp = new Scanner(System.in);
        System.out.println("Enter a number");
        int number = temp.nextInt();
        for (int i = 2; i < number; i++) {
            findPrime(i);
        }
    }

    static void findPrime(int num) {
        boolean isPrime = true;

        for (int i = 2; i < num; i++) {
            if (num % i == 0)
                isPrime = false;
        }

        if (isPrime == true)
            System.out.print(num + " ");

    }
}
