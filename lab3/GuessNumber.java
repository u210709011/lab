
import java.util.Random;
import java.util.Scanner;


public class GuessNumber {
	public static void main(String[] args) {
		Scanner temp = new Scanner(System.in);
		Random randTemp = new Random();

		int number = randTemp.nextInt(100);
		System.out.println(number);

		System.out.print("I'm thinking of a number between 0 and 99. You have 6 chances \nCan you guess it?..:");
		Integer guess = temp.nextInt();
		int chances = 5;

		while (guess != number && guess != -1 && chances > 0) {
			System.out.println("Sorry! You have " + (chances) + " chances left.");
			if (number > guess)
				System.out.println("Mine is greater than your guess");
			else
				System.out.println("Mine is smaller than your guess");
			System.out.print("Type -1 to quit or guess another..:");
			guess = temp.nextInt();
			chances--;
		}

		if (guess == number)
			System.out.println("Congratulations! You won after " + (6 - chances) + " attempts");
		else
			System.out.println("Sorry the number was " + number);

	}
}
