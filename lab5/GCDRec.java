public class GCDRec {
    public static void main(String[] args) {
        int int1, int2, myInteger1, myInteger2;

        try {
            int1 = Integer.parseInt(args[0]);
            int2 = Integer.parseInt(args[1]);


            myInteger1 = int1 >= int2 ? int1 : int2;
            myInteger2 = int1 <= int2 ? int1 : int2;

            while (true) {

                int r = myInteger1 % myInteger2;

                if (r == 0) {
                    System.out.println("GCD of " + int1 + " and " + int2 + " is " + myInteger2);
                    break;
                }

                myInteger1 = myInteger2;
                myInteger2 = r;

            }

        } catch (Exception e) {

            System.out.println("You can't find the GCD of 0!");

        }
    }
}
