public class FindMin {
    public static void main(String[] args ) {
        int value1;
        int value2;
        int value3;
        int result;

        value1 = Integer.parseInt(args[0]);
        value2 = Integer.parseInt(args[1]);
        value3 = Integer.parseInt(args[2]);

        result = value1 < value2 ? value1 : value2;
        result = result < value3 ? result : value3;

        System.out.println(result);

    }
}
