package generics;

import java.util.ArrayList;
import java.util.List;

public class StackArrayListImpl<T extends Number> implements Stack<T> {
    private List<T> stack = new ArrayList<T>();


    public void push(T item) {
        stack.add(item);
    }

    @Override
    public T pop() {
        if (stack.size()> 0)
            return stack.remove(stack.size()-1);
        return null;
    }

    @Override
    public boolean empty() {
        return stack.size() == 0;
    }

    @Override
    public List<T> toList() {
        return null;
    }

}

