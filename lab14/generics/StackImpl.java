package generics;

import java.util.ArrayList;
import java.util.List;

public class StackImpl<T extends Number> implements Stack<T> {

    private StackItem<T> top;

    @Override
    public void push(T item) {
        StackItem stackItem = new StackItem(item);
        stackItem.setNext(top);
        top = stackItem;
    }

    @Override
    public T pop() {
        if (top != null) {
            T item = top.getItem();
            top = top.getNext();

            return item;
        }
        return null;
    }

    @Override
    public boolean empty() {
        return top == null;
    }

    @Override
    public List<T> toList() {
        List<T> lst = new ArrayList<T>();


        StackItem<T> item = top;
        while(item!= null){
            lst.add(0, item.getItem());
            item = item.getNext();
        }
        return lst;
    }

}