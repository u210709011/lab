package generics;

public class TestStack {


    public static void main(String[] args){
        testStack(new StackImpl());

        testStack(new StackArrayListImpl<Number>());
    }

    public static void testStack(Stack<Number> stack){

        stack.push(5);
        stack.push(6);
        stack.push(2);
        stack.push(11);
        stack.push(23);

        System.out.println("stack 1: " + stack.toList());

        Stack<Integer> stack2 = new StackImpl<>();
        stack.push(44);
        stack.push(11);
        stack.push(55);


        System.out.println("stack 2: " + stack2.toList());

        stack.addAll(stack2);
        System.out.println();

        while (!stack.empty()){
            System.out.println(stack.pop());
        }

        System.out.println(stack.toList());
    }

}